##### Zahlensysteme, Binärrechnung

$101,011_2=1*2^2+0*2^1+1*2^0+0*2^{-1}+1*2^{-2}+1*2^{-3}\\\qquad\qquad=4+2+1+0,25+0,125$

|           | Binär:  | Hexadezimal:       | Dezimal:         |
| --------- | ------- | ------------------ | ---------------- |
| C         | `0b...` | `0x...`            | `...`            |
| Assembler | `...b`  | `0x...`  /  `...h` | `...`  /  `...d` |

|         | 1-Komplement                                                 | 2-Komplement                                             |
| ------- | ------------------------------------------------------------ | -------------------------------------------------------- |
| positiv | `0 [positive Dualzahl]`                                      | `0 [positive Dualzahl]`                                  |
| negativ | `1 [invertierte pos. Dualzahl]`                              | `[invertierte pos. Dualzahl + 1]`                        |
|         | + gut für Multiplikation / Division<br />– schlecht für Addition / Subtraktion | + keine doppelte Null<br />+ wird beim Rechnen verwendet |

```
1010110² *10101²:
					1 0 1 0 1 1 0		10101
				+	  0 0 0 0 0 0 0		 0101
				+	    1 0 1 0 1 1 0	  101
				+		  0 0 0 0 0 0 0	   01
				+		  	1 0 1 0 1 1 0	1
				—————————————————————————————
					1 1 1 0 0 0 0 1 1 1 0 ² = 1806¹⁰
```



##### Assembler-Befehle

```assembly
add AX, 4		; AX += 4
sub CX, 5		; BX -= 5
mov CX, BX		; CX = BX
inc CX			; CX += 1
dec CX			; CX -= 1
neg	DX			; DX *= -1

mul BX			; DX:AX = AX * BX		; mul immer mit Register!
mul ECX			; EDX:EAX = EAX * ECX

div BX			; AX = DX:AX / BX		Rest: DX
div ECX			; EAX = EDX:EAX / ECX	Rest: EDX
```

| Flag-Register | Name     | Bedeutung                                                   |
| ------------- | -------- | ----------------------------------------------------------- |
| C             | Carry    | Bereichsüberschreitung für vorzeichenlose Zahlen            |
| O             | Overflow | Bereichsüberschreitung für vorzeichenbehaftete Zahlen       |
| S             | Sign     | Ergebnis ist negativ (relevant für Zweierkomplement-Zahlen) |
| Z             | Zero     | Ergebnis ist Null (relevant für alle Zahlen)                |

```assembly
cmp AX,3		; if( AX-3 == 0 ) {Zero flag = 1}

je bzw jz		; if( AX = 3 ) {jump}
jne bzw jnz		; if( AX ≠ 3 ) {jump}

jc bzw jo		; if( C == 1 ) bzw if( O == 1 ) {jump}
jnc bzw jno		; if( C == 0 ) bzw if( O == 0 ) {jump}
```

| vorzeichenlose Zahlen                                        | vorzeichenbehaftete Zahlen                                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20200119100348213](PR1 Zusammenfassung.assets/image-20200119100348213.png) | ![image-20200119100403482](PR1 Zusammenfassung.assets/image-20200119100403482.png) |

```assembly
mov CX,i		;for(int CX=i; CX>0; CX--) {Befehle}
Schleife:
  ;Befehle
loop Schleife
```

```assembly
shr bzw shl		;Lücken werden mit 0 gefüllt					;Rest in Carry-Flag
sar bzw sal		;Lücken werden mit vorherigem MSB gefüllt		;Rest in Carry-Flag
```

```assembly
push 10			; [SP] = 10
call Prozedur	; SP -= 4 (bit von 10)		;[SP] = IP
Prozedur:
mov BP,SP		; BP = SP
push AX			; SP -= 2 (bit von AX)		; [SP] = AX
...
pop AX			; AX = [SP]					; SP += 2 (bit von AX)
ret				; IP = [SP]					; SP += 4 (bit von IP)
```

```assembly
mov [0],1011011b	;schreibt 2 in rechte Stelle von 7-Segment-Display
```



##### Datentypen

| Variable       | Storage size | Value range                     | Platzhalter `printf`   | Platzhalter `scanf` |
| -------------- | ------------ | ------------------------------- | ---------------------- | ------------------- |
| string         |              |                                 | `%s`                   | \t → tab            |
| char           | 1 byte       | -128 to 127                     | `%c`                   | \\" → "             |
| unsigned char  | 1 byte       | 0 to 255                        |                        | \\' → '             |
| short          | 2 bytes      | $-2^{15}$ to $2^{15}-1$         |                        | %% → %              |
| unsigned short | 2 bytes      | $0$ to $2^{16}$                 |                        |                     |
| int            | 4 bytes      | $-2^{31}$ to $2^{31}-1$         | `%i`                   | `%i`                |
| unsigned int   | 4 bytes      | $0$ to $2^{32}$                 | `%u` (dez)  `%x` (hex) |                     |
| long           | 8 bytes      | $-2^{63}$ to $2^{63}-1$         |                        |                     |
| unsigned long  | 8 bytes      | $0$ to $2^{64}$                 |                        |                     |
| float          | 4 byte       | ±1.401e-45<br />bis +3.403e+38  | `%f`                   |                     |
| double         | 8 byte       | ±4.941e-324<br />bis 1.798e+308 | `%f`                   | `%lf`               |

<img src="PR1 Zusammenfassung.assets/image-20200121120955865.png" alt="image-20200121120955865" style="zoom:50%;" />



##### Definitionen

| Wort                | Definition                                                   |
| ------------------- | ------------------------------------------------------------ |
| primitiver Datentyp | Ganze Zahlen, Kommazahlen, Schriftzeichen                    |
| Tautologie          | Ausdruck, der für alle Kombinationen der Wahrheitswerte seiner Variablen den Wahrheitswert 1 annimmt |
| Kontradiktion       | Ausdruck, der für alle Kombinationen der Wahrheitswerte seiner Variablen den Wahrheitswert 0 annimmt |
| Deklaration         | Variable wird bestimmter Datentyp zugeordnet                 |
| Initialisierung     | Variable wird bestimmter Wert gegeben                        |
| Definition          | Deklaration und Initialisierung                              |
| Operatoren          | z. B. "+", "-", "%", "*", "/"                                |
| Literal             | Bezeichnung für die Elemente eines Datentyps,  z.B. 17, -3, 5.78, 'a' |
| Emulation           | Interpreter lassen sich auch für die Übersetzung von Maschinensprachen einsetzen. Interpreter bildet dann einen bestimmten Rechnertyp auf einem anderen Rechnertyp nach |
| Interpretaiton      | Bei der Interpretation werden die Programmanweisungen von einem speziellen Programm einzeln in Maschinensprache übersetzt. Jede übersetzte Zeile wird direkt danach ausgeführt |
| dangling-else       | Bei mehreren If-Anweisungen und einer Else-Anweisung ist unklar, zu welcher If-Anweisung das Else gehört (Else gehört immer zu letztem If) |
| Baum                | besteht aus Knoten und Knaten, erster Knoten ist Wurzel, letzte Knoten Blätter |



##### Rechenbeispiele

| Bezeichnung              | Rechnung                                                     |
| ------------------------ | ------------------------------------------------------------ |
| Modulo / Rest            | 17 % 3 = 2        17 % -3 = 2        -17 % 3 = -2        -17 % -3 = -2 |
| normierte Gleitkommazahl | 384x10⁶ = 3,84x10⁸~10~ = 1,011011100011011E11100~2~          |
| Gleitkomma-Rechnung      | int tmp = 7.0/2.0  //tmp=3        float tmp = 7/2  //tmp=3.0<br />float tmp = 7.0/2  //tmp=3.5        ==(1/2 + 7.5) = 8.0 ???== |

![image-20200208075036409](./PR1 Zusammenfassung.assets/image-20200208075036409.png)



##### Standartfunktionen

| Bezeichnung                                                  | Funktion                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| exp(...);  /  sin(...)  /  cos(...)                          | e^...^                                                       |
| M_PI                                                         | $\pi$                                                        |
| log(...)  /  log10(...)  /  pow(x, y)                        | x^y^                                                         |
| fabs(...);                                                   | Betrag von ...                                               |
| strlen(const char *str);                                     | Länge der Zeichenkette str[]={"..."}                         |
| ![image-20200121162830586](PR1 Zusammenfassung.assets/image-20200121162830586.png) | ![image-20200121162730448](PR1 Zusammenfassung.assets/image-20200121162730448.png) |
| strstr(const char *s1, const char *s2);                      | Zeiger auf Stelle, an der Zeichenfolge 2 in Zeichenfolge 1 vorkommt |
| strchr(const char *str, int c);                              | Zeiger auf erste Stelle, an der c in Zeichenfolge vorkommt   |
| num%2 == 0 (1)                                               | Zahl ist gerade (ungerade)                                   |



##### Umlaute & Sonderzeichen

```c
#define myCharacter (unsigned char) 000
```

Ä: 142
ä: (unsigned char)132
Ö: (unsigned char)153
ö: (unsigned char)148
Ü: (unsigned char)154
ü: (unsigned char)129
ß: (unsigned char)225



##### Zeiger

```c
int i = 3;			//mov [100],3
int * p = &i;		//mov p,100
int j = *p			//mov j,3

i[j] /*entspricht*/ *(i+j);

*p++: Liefert Feldelement i, Pointer zeigt danach auf Element i+1
    
(*p)+1: Liefert Feldelement i erhöht um 1,Pointer danach immer noch auf i
    
(*p)++: Liefert Feldelement i und erhöht dieses danach um 1,
Pointer danach immer noch auf i
    
++*p und ++(*p): Liefern den inkrementierten Wert von Feldelement i. 
Pointer danach immer noch auf i
    
*++p: Erhöht Index auf Feldelement i+1 und liefert dessen Wert
```



##### Funktionen

```c
//Kein Rückgabewert				//Rückgabewert
void myFunktion(){				int myFunktion(int i){
    /*Befehle					...*/ return (i*2); return i;
}								}
```



##### Zeichenketten

```c
char str[]={"Hallo\n"}
/*——————————————
 H a l l o \n \0		→ 7 Elemente
 ———————————————*/

char inpstr[100];
gets(inpstr);			//ermöglicht Konsolen-Eingabe mit Leerzeichen
printf("Gelesener String: %s\n", inpstr);
```

​	

##### Bitoperationen

```c
int a=0b00010...;				//Links frei werdende Stellen werden mit VZ-bit gefüllt
a >> b;				//shr a,b	//Rechts frei werdende Stellen werden mit Null gefüllt
~a;					//mul a,-1
//Und	Oder	Exor
a & b;	a | b;	a ^ b;
```



##### Häufige Fehler

Umrechnung Zahlensysteme <u>immer</u> mit Rechenweg (Horner-Schema)
