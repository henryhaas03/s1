[TOC]



# 1. Differentialrechnung

$f(x)=a^x\quad→\quad f'(x)=a^x*\ln(a)\qquad\qquad f(x)=\ln(x)\quad→\quad F(x)=x*\ln(x)-x$



### 1.1 Fehlerrechnung

$f(x,y)$		$x=x_0 \pm dx$		$y=y_0 \pm dy$

| Absoluter linearer Größtfehler           | Relativer linearer Größtfehler | Allgemeine Formel  $f=C\ x^\alpha y^\beta$                   |
| ---------------------------------------- | ------------------------------ | ------------------------------------------------------------ |
| $df=\vert f_x*dx\vert+\vert f_y*dy\vert$ | $\frac{df}{f(x_0,y_0)}$        | $\frac{df}{f}=\vert \alpha*\frac{dx}{x}\vert +\vert \beta*\frac{dy}{y}\vert$ |
| physikalische Einheit                    | Prozent                        | Prozent                                                      |



### 1.2 Linearisierung & Taylorpolynome



**Eindimensional:**

| 1. Grad / Linearisierung                                     | $f(x)\approx f(x_0)+f'(x_0)*(x-x_0)$                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **2(+). Grad / Taylorpolynom**                               | $T_n(x)=f(x_0)+f'(x_0)(x-x_0)+\frac{f''(x_0)}{2!}(x-x_0)^2+\ ...\ +\frac{f^n(x_0)}{n!}(x-x_0)^n$ |
| [**Fehlerabschätzung:**](https://www.youtube.com/watch?v=yOvNpwUXbqI) | $\vert f(x)-Tay(x)\vert \ \le\ \frac 1{(n+1)!}\ ^{max}_y\vert f^{(n+1)}(y)\vert *\vert x-x_0\vert ^{n+1}$ |



**Mehrdimensional:**

| 1. Grad (totale Ableitung)        | $f(x,y)\approx f(x_0,y_0) + f_x(x_0,y_0)(x-x_0) + f_y(x_0,y_0)(y-y_0)$ |
| --------------------------------- | ------------------------------------------------------------ |
| **2(+). Grad (totale Ableitung)** | $f(x,y)\approx f(x_0,y_0) + f_x(x_0,y_0)(x-x_0) + f_y(x_0,y_0)(y-y_0)\\+\frac{f_{xx}(x_0,y_0)}{2!}(x-x_0)^2+\frac{f_{yy}(x_0,y_0)}{2!}(y-y_0)^2+f_{xy}(x_0,y_0)(x-x_0)(y-y_0)$ |
| andere Schreibweise               | $f_{xy}=\frac d{dx}*\frac d{dy}*f=\frac{d^2f}{dx\ dy}\qquad\qquad\frac{d^2f}{d(x,y)^2}=\begin{pmatrix}f_{xx}&f_{xy}\\f_{yx}&f_{yy}\end{pmatrix}$ |



### 1.3 Totale Ableitung 

$\frac{df}{d(x,y,z)}=(f_x,\ f_y,\ f_z)=\left(\frac{df}{dx},\ \frac{df}{dy},\ \frac{df}{dz}\right)$

| skalar                                                       | vektorielell                                                 | Gradient (Vektor totale Ableitung)                           |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\frac{df}{d\vec a}\bigg\vert _{\vec x}=\frac{df}{d(x,y)}\bigg\vert _{\vec x_0}*\frac{\vec a}{\vert \vec a\vert }\\\ \\=\begin{pmatrix}f_x(x_0,y_0)\\f_y(x_0,y_0)\end{pmatrix}\begin{pmatrix}x\\y\end{pmatrix}\frac 1{\sqrt{\vec x}}$ | $\vec f(\vec x)=\vec f(\vec x_0)+\frac{d\vec f}{d(x,y)}\bigg\vert _{\vec x_0}(\vec x-\vec x_0)\\\ \\\vec f(\vec x)=\vec f(\vec x_0)+\frac{d\vec f}{dx}\bigg\vert _{\vec x_0}(x-x_0)+\frac{d\vec f}{dy}\bigg\vert _{\vec x_0}(y-y_0)$ | Gradient zeigt in Richtung stärksten Anstiegs, Länge entspricht Steigung  $f'=\sqrt{f_x^2+f_y^2+f_z^2}$ |

 

### 1.4 Potenzreihen

$e^x=1+x+\frac 12x^2+\frac 16x^3\dots\qquad\cos(x)=1-\frac 12x^2+\frac 1{4!}x^4-\frac 1{6!}x^6\dots\qquad\sin(x)=x-\frac 16x^3+\frac 1{5!}x^5\dots$



**Polynom-Produkt:**

$f(x)=e^{-x}*\cos(2x)\quad→\quad Tay_2(x)=(1-x+\frac 12x^2)*(1-2x^2)$

```
1  -1  0,5  *  1   0  -2
––––––––––––––––––––––––
1  -1  0,5   |
	0	0    | 0
	   -2    | 2  -1
–––––––––––––|--------
1 –x –1,5x²  | Müll
```



**Umformung bei Entwick ==L== ==U== ngsstelle x $\ne$ 0  (==L==inks ==U==nten)**

Bei  $x=x_0\ne0$  erst Umformung	$f(x)=\frac{2x-4x^2}{2+x-2x^2}=\frac{-2-(x-1)-4(x-1)^2}{1-3(x-1)-2(x-1)^2}$  mit ausführlichem Horner-Schema

```
				| -2   1   2	hohe Potenzen vorn
Entwick-	––––|––––––––––––
lungs-		  1	|	  -2  -1
stelle		––––|––––––––––––
				| -2  -1 ‖ 1	← f(1)
			––––|––––––––––––
			  1	|	  -2
			––––|––––––––
				| -2 ‖ -3	← f'(1)
			 ...  -2	← 1/2! f''(1)
```



**P ==O== lynom-/ ==R== eihen-Division (Höchste Potenz ==R==echts ==O==ben):**

$f(x)=\underbrace{\frac{4+2x+2x^2}{2-2x+4x^2}}_{niedrigste\ Potenz\\muss\ 1\ sein}=\frac{2+x+x^2}{1-1x+2x^2}\approx\underbrace{2+3x+0x^2-6x^3}_{Tay_3(x)}-6x^4\dots$

```
				  hohe Potenzen hinten
hohe Poten-	  
zen oben			| 2  1  1  0  0		← fehlende Potenz durch Null darstellen
				––––|––––––––––––––––
'1' nicht		 -2 |	   -4 -6  0 ..
notieren		 +1 |	 2  3  0 -6 ..
				––––|––––––––––––––––
umgekehrte			| 2  3  0 -6 -6 ...
Vorzeichen
```



**Laurentreihen**

Pol im Nenner ausklammern:	$f(x)=\frac{1-x+2x^3}{x+2x^2}=\frac 1x\frac{1-x+2x^3}{1+2x} \qquad f(x)=\frac{2+2(x-1)}{2(x-1)^2+(x-1)^3}=\frac 1{2(x-1)^2}\frac{2+2(x-1)}{1+0,5(x-1)}$

Bei Polynom-/Reihen-Division muss Grad des Ergebnis-Polynoms um Grad des ausgeklammerten Nenners größer sein

<div style="page-break-after: always;"></div>

### 1.5 Grenzwerte

|        x ⇒ ±∞        | $x→+\infty$             | $x→-\infty$                       |
| :------------------: | ----------------------- | --------------------------------- |
|  höchstes Wachstum   | $2^x,e^x$               | $→0$                              |
|          ↓           | $x^2,\sqrt x$           | $→\pm\infty$  ((un)gerade Potenz) |
|          ↓           | $\ln(x), \log_2(x)$     | geht nicht!                       |
| niedrigstes Wachstum | $\cos(10x), \sin(x), 4$ | bleibt gleich                     |

| **x ⇒ ±∞**                                                   |
| ------------------------------------------------------------ |
| $\lim\limits_{x→x_0}\frac{f(x)}{x-x_0}=nicht\ definiert\qquad\lim\limits_{x→x_0^+}\frac{f(x)}{x-x_0}=+\infty\qquad\lim\limits_{x→x_0^-}\frac{f(x)}{x-x_0}=-\infty$ |
| **Regel von l'Hospital**   bei  $f(x_0)=0/\infty,\ g(x_0)=0/\infty:\qquad\lim\limits_{x→x_0}\frac{f(x)}{g(x)}=\lim\limits_{x→x_0}\frac{f'(x)}{g'(x)}$ |





# 2. Integralrechnung



### 2.1 Wichtige Formeln

| Name                      | Formel                                                       |
| ------------------------- | ------------------------------------------------------------ |
| Volumen Rotationskörper   | x-Achse:  $V=\pi*\int_a^b\ (f(x))^2\ dx$<br />y-Achse:  $V=\pi*\int_a^b\ x^2f'(x)\ dx$ |
| Länge Kurve               | $l=\int_a^b\ \sqrt{1+f'(x)^2}\ dx$                           |
| Arithmetischer Mittelwert | $\bar v=\frac 1{(b-a)}*\int_a^b v(t)\ dt$                    |
| Effektivwert              | $f_{eff}=\sqrt{\frac 1{(b-a)}*\int_a^b f(t)^2\ dt}$          |



### 2.2 Integration über Weg/Kurve


$$
\int_s \vec f(\vec x(t))\ d\vec s\qquad\qquad \int_sf(\vec x(t))\ ds
$$


|                      | vektoriell                                                   | skalar                                                    |
| -------------------- | ------------------------------------------------------------ | --------------------------------------------------------- |
| summierte Teilstücke | $\vec f(x,y)=\left(\begin{array}{c} f_1(x,y) \\ f_2(x,y) \end{array}\right)$ | $f(x,y)$                                                  |
| Beschreiben          | $\vec x(t)=(t,g(t))\quad t_1\leq t\leq t_2$                  | $\vec x(t)=(t,g(t))\quad t_1\leq t\leq t_2$               |
| Element              | $\dot{\vec x}(t)\ dt$                                        | $\vert \dot{\vec x}(t)\vert \ dt$                         |
| Übersetzen           | $\vec f(\vec x(t))=\vec f(t,g(t))=\left(\begin{array}{c} f_1(t,g(t)) \\ f_2(t,g(t)) \end{array}\right)$ | $f(t,g(t))$                                               |
| Lösen                | $\int_{t_1}^{t_2}\vec f(\vec x(t))*\dot{\vec x}(t)\ dt$      | $\int_{t_1}^{t_2}f(g(t))*\vert \dot{\vec x}(t)\vert \ dt$ |
| EXTRA:               | *Bei Potenzial:*  $\int_{t_1}^{t_2}\vec f(\vec x(t))*\dot{\vec x}(t)\ dt\ =\ V(\vec x(t_2))-V(\vec x(t_1))$ | *Länge der Kurve:  $f(g(t))=1$*                           |



### 2.3 Integration über Fläche

|                      | vektoriell                                                   | skalar                                                       |
| -------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| summierte Teilstücke | $\vec f(x,y)=\left(\begin{array}{c} f_1(x,y) \\ f_2(x,y) \end{array}\right)$ | $f(x,y)$                                                     |
| Beschreiben          | $\vec x(\alpha, \beta)\quad+\quad\text{Grenzen}$             | $\vec x(\alpha, \beta)\quad+\quad\text{Grenzen}$             |
| Element              | $d\vec A=\left(\frac{d\vec x}{d\alpha}\times\frac{d\vec x}{d\beta}\right)\ d\alpha\ d\beta$ | $dA=\left\vert \frac{d\vec x}{d\alpha}\times\frac{d\vec x}{d\beta}\right\vert \ d\alpha\ d\beta$ |
| Übersetzen           | $\vec f(\vec x(\alpha,\beta))$                               | $f(\vec x(\alpha,\beta))$                                    |
| Lösen                | $\int\int\ \vec f(\vec x(\alpha,\beta))*\vec n(\alpha,\beta)\ \ d\alpha\ d\beta$ | $\int\int\ f(\vec x(\alpha,\beta))*\vert \vec n(\alpha,\beta)\vert \ \ d\alpha\ d\beta$ |



### 2.4 Integration über Volumen

|                 | Kartesisch   | Zylinder                            | [Kugel](https://www.youtube.com/watch?v=4unatZKE5r8&t=149s)  |
| --------------- | ------------ | ----------------------------------- | ------------------------------------------------------------ |
| Volumen-element | $dx\ dy\ dz$ | $r\ dr\ d\varphi\ dz$               | $r^2sin(\vartheta)\ dr\ d\varphi\ d\vartheta$                |
| Koordinaten     | $(x,y,z)$    | $(r\cos(\varphi),r\sin(\varphi),z)$ | $(r\cos(\varphi)*\sin(\vartheta),r\sin(\varphi)*\sin(\vartheta),r\cos(\vartheta))$ |



### 2.5 Integrationstechniken



[**Partielle Integration** (lineare Verkettung):](https://youtu.be/AWN01OjmgWI)	$\int g'*h=g*h-\int g*h'$

| Funktion    | Integration:  $e^x,\ \sin(x),\ \cos(x)$ | Ableitung:  $\ln(x),\ \arctan(x)\ →\ \frac 1{1+x^2}$ |
| ----------- | --------------------------------------- | ---------------------------------------------------- |
| **Polynom** | Ableitung                               | Integration                                          |



[**Substitution ohne Grenzen** (Verkettung höheren Grades)](https://www.youtube.com/watch?v=rKGlE4av4-c&t=187s)

$\int\ Pol(x)*g(h(x))\ dx\underbrace=_{dx=\frac 1{h'(x)}du} \int\ \frac{Pol(x)}{h'(x)}*g(u)\ du=...=n*G(u)\underbrace=_{u=h(x)}n*G(h(x))$

$u=h(x)\quad→\quad \frac{du}{dx}=h'(x)\quad\Leftrightarrow\quad dx=\frac 1{h'(x)}du$



**Substitution mit Grenzen**

$\int_{x_1}^{x_2}\ Pol(x)*g(h(x))\ dx\underbrace=_{u=h(x)\quad→\quad dx=\frac 1{h'(x)}du} \int_{u_1}^{u_2}\ \frac{Pol(x)}{h'(x)}*g(u)\ du=...=n*\big[G(u)\big]_{u_1}^{u_2}\underbrace=_{u=h(x)}n*G(h(x))$

$u_1=h(x_1)\qquad u_2=h(x_2)$



[**Partia ==L== br ==U== chzerlegung  (Höchste Potenz ==L==inks ==U==nten)**](https://youtu.be/t7PqzaHKFHw)

$\frac{3x^3-4x^2-x-4}{x^2-2x-1}=(3x-1)*\frac{4x-6}{x^2-2x-1}\qquad\qquad \int \frac 1{a^2+x^2}dx=\frac 1a\arctan(\frac xa)$	(Formelblatt)

```
				  hohe Potenzen vorn
umgekehrte	  
Vorzeichen			| 3 -4  -1 -4		← fehlende Potenz durch Null darstellen
				––––|–––––––––––––
'1' nicht		 +2 |	     6 -2
notieren		 +1 |	 3  -1			← Lücke lassen !
				––––|–––––––––––––
hohe Poten-			| 3 -1 ‖ 4 -6		← Vorfaktor ‖ Zähler
zen unten			 x1 x0   x1 x0
```



### 2.6 Integralätze

| [Green: Zweidimensionale geschlossene Kurve](https://www.youtube.com/watch?v=krzWPY9cy7o) | [Stockes: Dreidimensionale Kurve](https://www.youtube.com/watch?v=1_ZA7_LYSYE&t=793s) | [Gauß: Dreidimensionale Fläche](https://www.youtube.com/watch?v=9nXs-l1OBmk) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\vec f(x,y)=\begin{pmatrix}f_1(x,y)\\f_2(x,y)\end{pmatrix}$ | $\vec f(x,y,z)=\begin{pmatrix}f_1(x,y,z)\\f_2(x,y,z)\\f_3(x,y,z)\end{pmatrix}$ | $\vec f(x,y,z)=\begin{pmatrix}f_1(x,y,z)\\f_2(x,y,z)\\f_3(x,y,z)\end{pmatrix}$ |
|                                                              | $rot(\vec f)=\begin{pmatrix}\frac d{dx}\\\frac d{dy}\\\frac d{dz}\end{pmatrix}\times\begin{pmatrix}f_1\\f_2\\f_3\end{pmatrix}$ | $div(\vec f)=\begin{pmatrix}\frac d{dx}\\\frac d{dy}\\\frac d{dz}\end{pmatrix}*\begin{pmatrix}f_1\\f_2\\f_3\end{pmatrix}$ |
| $\oint\vec f\ d\vec x=\int\int_A\frac d{dx}f_2-\frac d{dy}f_1\ dA$ | $\oint\vec f\ d\vec x=\int\int_A rot\ \vec f\ d\vec A$       | $\oiint	\vec f\ d\vec x=\iiint div(\vec f)\ dV$           |
| K läuft gegen Uhrzeigersinn, sonst VZW                       | Rechte Hand:  Daumen - Normalenvektor / Finger - Kurvenrichtung | div(f) positiv: Quelle      div(f) negativ: Senke            |





# 3. Lineare Algebra



### 3.1 Vektoren, [Matrizen](https://youtu.be/3gMJ3fd-h88) und Determinanten

|                                                              | paralleler & orthogonaler Anteil                             | [Inverse](https://www.youtube.com/watch?v=jGHTVeJ0xto&t=126s) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20191210153134146.png" alt="image-20191210153134146" style="zoom: 40%;" /> | $\vec a_\bot=\vec a-\vec a_\parallel\qquad\vec a_\parallel=\frac{\vec a·\vec b}{\vec b·\vec b}·\vec b$ | $\begin{pmatrix}a&b\\c&d\\ \end{pmatrix}^{-1}=\frac 1{ad-bc}\begin{pmatrix}d&-b\\-c&a\end{pmatrix}$ |



[**Umformen Matrizen**](https://youtu.be/Yh9Vp0ZOimc)

$(P_1·P_2)^{-1}=P_2^{-1}·P_1^{-1}$



| Umformen Determinante                                        | Multiplizieren Matrix / Determinante                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\begin{vmatrix}1&2&0\\3&3&2\\1&2&3\end{vmatrix}=+1·\begin{vmatrix}3&2\\2&3\end{vmatrix}-2·\begin{vmatrix}3&2\\1&3\end{vmatrix}$ | $2*\begin{pmatrix}1&2\\3&4\end{pmatrix}=\begin{pmatrix}2&4\\6&8\end{pmatrix}\\\ \\2*\begin{vmatrix}1&2\\3&4\end{vmatrix}=\begin{vmatrix}2&4\\3&4\end{vmatrix}=\begin{vmatrix}2&2\\6&4\end{vmatrix}$ |



### 3.2 Lineare Gleichungssysteme

| Name                | Regel                                                        | Vor- / Nachteile                                             |
| ------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Cramersche Regel    | $\begin{matrix}ax+by=E\\cx+dy=F\end{matrix}\qquad x=\frac{\begin{vmatrix}E&b\\F&d\end{vmatrix}}{\begin{vmatrix}a&b\\c&d\end{vmatrix}}\qquad y=\frac{\begin{vmatrix}a&E\\b&F\end{vmatrix}}{\begin{vmatrix}a&b\\c&d\end{vmatrix}}$ | - nur für 2x2 und 3x3<br />- Systemd.= 0: unmögl.<br />+ Kommazahlen möglich |
| Gauß-Jordan         | Nullstellen erzeugen                                         | - kann schlechte Kondition erzeugen                          |
| QR-Verfahren        | $LGS\ →\ x\ \vec a+y\ \vec b=\vec c\\\ \\1.\ \vert \vec a\vert=r\qquad\vec a'=\begin{pmatrix}(-)r\\0\end{pmatrix}\\2.\ \vec a-\vec a'=\vec r\quad\text{(ggf. noch kürzen)}\\3.\ \vec b\quad→\quad\vec b'=\vec b-2*\frac{\vec b*\vec r}{\vec r*\vec r}*\vec r\\\quad\vec c\quad→\quad\vec c'=\vec c-2*\frac{\vec c*\vec r}{\vec r*\vec r}*\vec r\\4.\ x\ \vec a'+y\ \vec b'=\vec c'$ | + Kondition bleibt erhalten                                  |
| Iterationsverfahren | Jakobi / Gesamtschritt<br />Gauss-Seidel / Einzelschritt     | - muss Spalten- oder Zeilendominant sein<br />+ funktioniert bei Dezimallösung |



### 3.3 Vektorräume

| linear abhängig                                              | linear unabhängig (BASIS)                        |
| ------------------------------------------------------------ | ------------------------------------------------ |
| $\{\vec x,\ \vec y,\ \vec z\}\quad→\quad a\ \vec x+b\ \vec y+c\ \vec z=\vec 0$ | sind nicht durcheinander darstellbar             |
| bei 3x3:$\qquad(\vec x\times\vec y)*\vec z= 0$               | bei 3x3:$\qquad(\vec x\times\vec y)*\vec z\ne 0$ |



### 3.4 lineare Ausgleichsrechnung

$P_1(x_1,y_1)\qquad P_2(x_2,y_2)\qquad P_3(x_3,y_3)$

| Ausgleichsgerade                                             | Standartschätzfehler                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\vec y=a*\vec x+b*\vec 1$		$\vec x\ \vec y=a*\vec x\ \vec x+b*\vec x\ \vec 1\\\vec 1\ \vec y=a*\vec 1\ \vec x+b*\vec 1\ \vec 1$ | $FQS=\vec y\ \vec y-a*\vec y\ \vec x-b*\vec y\ \vec 1\\SSF=\sqrt{\frac{FQS}{Anzahl\ Pkt.\ -\ Anzahl\ Param.}}=\sqrt{\frac{FQS}{3-2}}$ |



### 3.5 Projektion

$\vec x$  wird als  $\vec x_p$  auf Ebene von  $\vec a$  und  $\vec b$  projiziert

| Basisvektoren sind nicht orthogonal:$\quad\vec b_1*\vec b_2\ne0$ | Regel                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1. Wunschgleichung                                           | $\vec x=\alpha*\vec a+\beta*\vec b$                          |
| 2. mit allen Basisvektoren multiplizieren + LGS lösen<br />$a_1*\alpha+b_1*\beta=c_1\\a_2*\alpha+b_2*\beta=c_2$ | $\begin{matrix}&\vert&\vec a&\vec b&\vec x\\-&-&-&-&-\\\vec a&\vert&a_1&b_1&c_1\\\vec b&\vert&a_2&b_2&c_2\end{matrix}$ |
| 3. Variablen  $\alpha$  und  $\beta$  in Wunschgleichung einsetzen |                                                              |

| Basisvektoren sind orthogonal:$\quad\vec b_1*\vec b_2=0$     |
| ------------------------------------------------------------ |
| $\vec x_p=\vec b_{1\parallel}+\vec b_{2\parallel}=\frac{\vec x*\vec b_1}{\vec b_1*\vec b_1}*\vec b_1+\frac{\vec x*\vec b_2}{\vec b_2*\vec b_2}*\vec b_2=a*\vec b_1+b*\vec b_2$ |



### [3.6 Orthogonalisieren mit Gram-Schmidt](https://www.youtube.com/watch?v=l6pr1W3MQoE&t=561s)

| Vektoren                                                     | Funktionen                                                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\{\vec v_1,\ \vec v_2,\ \vec v_3\}\qquad\vec b_1=\vec v_1\qquad\vec b_2=\vec v_2-\frac{\vec v_2*\vec b_1}{\vec b_1*\vec b_1}*\vec b_1\\\ \\\vec b_3=\vec v_3-\frac{\vec v_3*\vec b_1}{\vec b_1*\vec b_1}*\vec b_1-\frac{\vec v_3*\vec b_2}{\vec b_2*\vec b_2}*\vec b_2\\$<br />(kürzen der Basisvektoren ist immer erlaubt) | $U=\lang U_1,U_2\rang\qquad B_1=U_1\\\ \\ B_2=U_2-\frac{\lang U_2,U_1\rang}{\lang U_1,U_1\rang}*U_1$ |



### 3.7 Affine Abbildungen

$\vec x'=A\ \vec x+\vec b=\begin{pmatrix}a&b\\c&d\end{pmatrix}\begin{pmatrix}x\\y\end{pmatrix}+\begin{pmatrix}e\\f\end{pmatrix}$

| Flächeninhalt                                                | Umkehrabbildung                                 | Fixpunkt/-gerade                 |
| ------------------------------------------------------------ | ----------------------------------------------- | -------------------------------- |
| $A_{Dreieck}=\frac 12\vert \vec x_1\times\vec x_2\vert \qquad A'_{Dreieck}=A_{Dreieck}*\vert A\vert $ | $\vec x'=A\ \vec x\qquad\vec x=A^{-1}\ \vec x'$ | $\vec x=A\ \vec x\quad$LGS lösen |



### [3.8 Eigenwert und Eigenvektor](https://www.youtube.com/watch?v=YMC_-87gbR8)

$(A-\lambda\ E)*\vec x=\vec 0\qquad\vert A-\lambda\ E\vert =0$

| 2x2                       | $\lambda^2-Spur(A)\ \lambda+det(A)=0\quad→\quad\lambda_1,\ \lambda_2$ |
| ------------------------- | ------------------------------------------------------------ |
| **3x3**                   | $-\lambda^3+Spur(A)\ \lambda^2-H_2 \lambda+det(A)=0\quad→\quad\lambda_1,\ \lambda_2,\ \lambda_3$ |
| nicht 2/3 verschiedene EW | $A-\lambda_1\ E=\begin{pmatrix}a&b\\c&d\end{pmatrix}\qquad\begin{matrix}a\ x_1+b\ x_2=0\\c\ x_1+d\ x_2=0\end{matrix}\qquad\alpha\begin{pmatrix}a\\-b\end{pmatrix}\\A-\lambda_2\ E=\begin{pmatrix}a&b\\c&d\end{pmatrix}\qquad\begin{matrix}a\ x_1+b\ x_2=0\\c\ x_1+d\ x_2=0\end{matrix}\qquad\beta\begin{pmatrix}a\\-b\end{pmatrix}$ |
| **2x2** verschiedene EW   | $A-\lambda_1\ E=\begin{pmatrix}a&k\ a\\c&k\ c\end{pmatrix}\qquad\alpha\begin{pmatrix}b\\d\end{pmatrix}\\A-\lambda_2\ E=\begin{pmatrix}b&k\ b\\d&k\ d\end{pmatrix}\qquad\beta\begin{pmatrix}a\\c\end{pmatrix}$ |
| **3x3** verschiedene EW   | $(A-\lambda_2E)*(A-\lambda_3E)=\begin{pmatrix}a&k\ a&n\ a\\b&k\ b&n\ b\\c&k\ c&n\ c\end{pmatrix}\qquad\alpha\begin{pmatrix}a\\b\\c\end{pmatrix}\\...$ |



### [3.9 Diagonalisierung / Matrizen potenzieren](https://www.youtube.com/watch?v=KmFq0Pl2nxM&t=335s)

Matrix  $A=\begin{pmatrix}x_1&x_2\\x_3&x_4\end{pmatrix}$  mit Eigenwerten  $\lambda_1$  und  $\lambda_2$  und Eigenvektoren  $\begin{pmatrix}a\\c\end{pmatrix}$  und  $\begin{pmatrix}b\\d\end{pmatrix}$  ergibt:

| Diagonalmatrix                                           | S-Matrix                                 | S⁻¹-Matrix                                                   | potenzierte Matrix A |
| -------------------------------------------------------- | ---------------------------------------- | ------------------------------------------------------------ | -------------------- |
| $D=\begin{pmatrix}\lambda_1&0\\0&\lambda_2\end{pmatrix}$ | $S=\begin{pmatrix}a&b\\c&d\end{pmatrix}$ | $S^{-1}=\frac 1{ad-bc}\begin{pmatrix}d&-b\\-c&a\end{pmatrix}$ | $A^n=S*D^n*S^{-1}$   |



<div style="page-break-after: always;"></div>

# 4. Weitere Themen



### 4.1 Interpolation

<u>Grad</u> Polynom = Anzahl der Punkte – 1



[**Lagrange:**](https://www.youtube.com/watch?v=9P3s0ULxxa0)

$p(x_1)=y_1\qquad p(x_2)=y_2\qquad p(x_3)=y_3$

$pol(x)=y_1\frac{(x-x_2)(x-x_3)}{(x_1-x_2)(x_1-x_3)} + y_2\frac{(x-x_1)(x-x_3)}{(x_2-x_1)(x_2-x_3)} + y_3\frac{(x-x_1)(x-x_2)}{(x_3-x_1)(x_3-x_2)}$



[**Newton:**](https://www.youtube.com/watch?v=p0W0JB6NXm8)

$p(x_1)=y_1\qquad p(x_2)=y_2\qquad p(x_3)=y_3$

$pol(x)=y_1+a_1(x-x_1)+b_1(x-x_1)(x-x_2)$

```
 x-Werte	 y-Werte
	x1		  → Y1 ←
						(y2–y1)/(x2–x1) = → A1 ←
	x2			y2									(a2–a1)/(x3–x1) = → B1 ←
						(y3–y2)/(x3–x2) = a2
	x3			y3
```



### [4.2 Partialbruchzerlegung](https://www.youtube.com/watch?v=UNPoNUc5hiE&t)

|                      | Nenner in LF zerlegen                                        | Gleichung * HN                                |                                                              |
| -------------------- | ------------------------------------------------------------ | --------------------------------------------- | ------------------------------------------------------------ |
| einfache LF          | $\frac{ax+b}{x^2+cx+d}=\frac{ax+b}{(x-x_1)(x-x_2)}\\=\frac A{(x-x_1)}+\frac B{(x-x_2)}$ | $ax+b=A*(x-x_2)+B*(x-x_1)$                    | $A=\frac{ax+b}{(x-x_2)}\bigg\vert_{x=x_1}\\B=\frac{ax+b}{(x-x_1)}\bigg\vert_{x=x_2}$ |
| mehrfache LF         | $\frac{u(x)}{v(x)}=\frac{u(x)}{(x-x_1)(x-x_2)^2}\\=\frac A{x-x_1}+\frac B{x-x_2}+\frac C{(x-x_2)^2}$ | $u(x)=A(x-x_2)^2+B*(x-x_1)\\(x-x_2)+C(x-x_2)$ | A & C wie oben, B mit x-Wert                                 |
| irreelle Nullstellen | $\frac{u(x)}{v(x)}=\frac{u(x)}{(x-x_1)(x^2+x_2)}\\=\frac A{x-x_1}+\frac {Bx+C}{x^2+x_2}$ | $u(x)=A(x^2+x_2)^2\\\qquad+(Bx+C)(x-x_1)$     | Koeffizienten-Vergleich                                      |



### [4.3 Sehnentrapez und Extrapolation](https://www.youtube.com/watch?v=BZbzdsvpc3c)

Schrittweite = 0,5^Fehler			$h=(\frac 12)^p$

$E:$  exakte Größe			$N(h):$  Näherungswert für  $E$  bei Schrittweite  $h$			$p:$  Fehlerordnung

| Extrapolation                                    | Integration                                                  |
| ------------------------------------------------ | ------------------------------------------------------------ |
| $f'(x)=2N(\frac h2)-N(h)$                        |                                                              |
| $f'(x)=\frac 43N(\frac h2)-\frac 13N(h)$         | schlechte F.  $A\approx h*(f(x_0)+f(x_1)...+f(x_{n-1}))=N(h_0)$ |
| $f'(x)=\frac{16}{15}N(\frac h2)-\frac 1{15}N(h)$ | gute F.  $A\approx \dfrac h2*\bigg(f(x_0)+2*\big[f(x_1)...+f(x_{n-1})\big]+f(x_n)\bigg)=N(\frac{h_0}2)$ |



### 4.4 Transformation von Funktionen

| Epsilon                                                      | Rechteck                                                     | Delta                                                        | Kamm                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20200118152248633](../../Typora.assets/image-20200118152248633.png) | ![image-20200118152314894](../../Typora.assets/image-20200118152314894.png) | ![image-20200118152358567](../../Typora.assets/image-20200118152358567.png) | ![image-20200118152424669](../../Typora.assets/image-20200118152424669.png) |



### [4.5 Newtonverfahren für nichtlineare Gleichungen](https://www.youtube.com/watch?v=JdMVcHUfAs0)

| Eindimensional                        | Zweidimensional                                              |
| ------------------------------------- | ------------------------------------------------------------ |
| $x_{n+1}=x_n-\dfrac{f(x_n)}{f'(x_n)}$ | $\begin{pmatrix}x_{n+1}\\y_{n+1}\end{pmatrix}= \begin{pmatrix}x_n\\y_n\end{pmatrix}- \begin{pmatrix}f_x&f_y\\g_x&g_y\end{pmatrix}^{-1} \bigg\vert_{(x_n,y_n)}* \begin{pmatrix}f\\g\end{pmatrix}\bigg\vert_{(x_n,y_n)}$ |



<div style="page-break-after: always;"></div>

# 6. Häufige Fehler

Aufgabe vollständig lesen!!!				Zahlenwerte <u>genau</u> abschreiben!!!



### Differentialrechnung

für Funktion der Form  $f=C\ x^\alpha y^\beta$  die Abkürzung verwenden

bei Berechnung der Geschwindigkeit aus "Beschleunigung mit Startgeschwindigkeit" immer Variable $C$ ausrechnen:	$a(t)=2t,\ v_0=10\qquad v(t)=t^2+C=t^2+10$

Interpretation Linearisierung:	"erhöht man  $var1$  um $0,1$ , so ändert sich  $var2$  um  $0,03$
Nicht Einheit pro Änderung, sondern nur Einheit & kein neg. Vorzeichen	$-2,5\frac Nm\quad→\quad2,5N$

Polynomdivision:  Nullen bei Horner-Schema nicht vergessen!
								 bei Kürzen um einzelne  $+1$  zu erzeugen auch in Nenner alle Vorzeichen umdrehen!

Grenzwerte:	Klammern <u>immer</u> zuerst ausmultiplizieren!





### Integralrechnung

Bei mit Einheit beschrifteten Achsen das Ergebnis auch mit Einheit angeben:	$V=40\pi\ cm^3$

bei uneigentlichem Integral nicht über Polstelle hinweg integrieren

wenn möglich ausmultiplizieren statt partiell integrieren:	$\int \frac{1-u}{\sqrt u}du=\int \frac 1{\sqrt u}du+\int\sqrt u\ du$

bei Schwerpunkt auf Faktor achten:	$A=\frac {32}3\qquad\int\int_Ay\ dA=\frac ab*32\qquad→\quad y_s=\frac 3{32}*32*\frac ab$

Bei Integration mit Geschwindigkeit nicht das C vergessen

Trägheitsmoment:	"homogen" bedeutet nicht, dass  $\varrho$  wegfällt, sondern nur, dass es vor das Integral geschrieben werden kann:	$\Theta_{zz}=\int_V\varrho(x^2+y^2)dV=\varrho\int_V(x^2+y^2)dV=\frac{64}3\pi*\varrho$



### Lineare Algebra

QR-Verfahren:	Nicht die Projektion des Ergebnis-Vektors vergessen!
QR:	GS am Ende nach Variablen auflösen:	$x=x_0,\ y=y_0\ ...$

Volumen Spat <u>mit</u> Betragsstrichen (Volumen kann nie negativ sein):	$V=\vert(\vec a\times\vec b)*\vec c\vert$

immer zuerst Wunschgleichung aufschreiben & Gleichung mit fertigen Variablen hinschreiben

Affine Abbildung:	<u>alle</u> Punkte abbilden

EW / EV Aufgabenstellung <u>genau</u> lesen!

erst FQS, dann SSF!



### Rechenfehler

$\sin(1)\ne1$

$\int e^{4x}dx=\frac 14e^{4x}\ne\frac 15e^{5x}$

