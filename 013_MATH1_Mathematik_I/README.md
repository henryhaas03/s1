# Links
## Klausuren
- [alte Klausuren](https://fs.hs-ulm.de/public/max/Download/klausuren_m1/)
## Übungen
- [Übungen Thomas Hartmann](https://fs.hs-ulm.de/public/hartmann/ET1)
## Unterlagen
- [Unterlagen Max Riederle](https://fs.hs-ulm.de/public/max/Download/mathe1)
