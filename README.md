# 1. Semester
## Inhalt
1. [011_ETGR_Elektrotechnik_I](https://gitlab.com/thulm/et/s1/-/tree/main/011_ETGR_Elektrotechnik_I)
2. [012_PROGC_Programmieren_in_C](https://gitlab.com/thulm/et/s1/-/tree/main/012_PROGC_Programmieren_in_C)
3. [013_MATH1_Mathematik_I](https://gitlab.com/thulm/et/s1/-/tree/main/013_MATH1_Mathematik_I)
4. [014_PHYS_Physik_I](https://gitlab.com/thulm/et/s1/-/tree/main/014_PHYS_Physik_I)
5. [015_DIGT_Digitaltechnik_I](https://gitlab.com/thulm/et/s1/-/tree/main/015_DIGT_Digitaltechnik_I)

# WhatsApp Gruppen
0. [Best of Ulm ET/DM](https://chat.whatsapp.com/GpukpodzFw9DVG76j4kldC)
1. Semester ET1/2d @darulin007 @ThorbenCarl
2. Semester ET2/3d @meixnerluk
3. [Semester ET3/6d](https://chat.whatsapp.com/GDVDNlR8O1xLIwYq4idENv)
4. Semester ET4/7d @Dani_13_03
5. [Semester ET5](https://chat.whatsapp.com/FvOVZEMvMEOKpq3C9J36Sr)
6. [Semester ET6/8d](https://chat.whatsapp.com/Dd2M6LO4qoP7qmLet0O3mA)
7. Semester ET7/9d @darulin007
- Die Gruppen wachsen über die Semester mit, Links bitte entsprechend weiterschieben. Duale Studenten wechseln dann in die entsprechenden Gruppen.

# Mitwirken? Unbedingt!
Die Repos sind von Studierenden für Studierenden, jeder profitiert vom anderen! Somit ist jeder eingeladen mitzuwirken und Inhalte zu ergänzen und Fehler zu berichtigen. Damit das auch gut funktioniert gibt es hier ein paar Informationen.
## Fehler gefunden? Verbesserungen? Ergänzungen?
Du hast einen Fehler gefunden, Verbesserungsvorschläge oder ergänzende Dokumente/Erklärungen? Dann wird es Zeit zum Handeln:
### Issues erstellen
Im Issue (Ticket) kannst du auf einen Fehler hinweisen oder melden, wenn etwas fehlt. Auch z.B., wenn du gerade keine Zeit hast es zu verbessern.
### Änderungen erstellen
Aus einem Issue erstellst du einen neuen Branch und pushest deine Änderungen. Anschließend mergest du deinen branch wieder in den main. 
Wenn du das Arbeiten mit git noch nicht kennst, gibt es eine Vielzahl an Tutorials im Internet. Bei fehlenden Rechten oder Fragen, fragt einfach deinen Semesterbetreuer.
### Welche Dokumente soll ich pushen?
Alles ist willkommen was andern weiter hilft. Achte darauf das immer ein PDF deines Dokumentes zur Verfügung steht. Nach Möglichkeit schreibe die Lösung nicht gleich unter die Aufgabe, jedoch z.B. an dem Ende des Dokuments so das man alles zusammen in einem Dokument hat. 
Markdown und LaTeX eigenen sich besonders gut. (Bei LaTeX bitte nur die Dateien die zum Bauen des Dokumentes benötigt werden, meisten nur .tex Dateien)
### Was darf nicht gepushed werden?
Alle Dokumente an den du keine Rechte hast. Solange ein Prof. nicht ausdrücklich zur Veröffentlichung zustimmt, ist die Veröffentlichung strengstens verboten. 
Es ist auch nicht das Ziel Dokumente zu duplizieren, heißt alles was von der THU veröffentlicht wird, egal ob Laufwerke, Moodle, … hat hier nichts verloren.

#### Metadaten 
Möchtest du deine Metadaten aus deinen PDF entfernen? Nutze das von @olagino bereitgestellte Skript: [remMeta.sh](https://gitlab.com/thulm/et/s1/-/blob/main/remMeta.sh)

## Repo Struktur und Dateinamen
```
└──0_Semester_X_LSFKurzbezeichnung_Name/Beschreibung_(Römischenummer) (Bsp.: 014_PHYS_Physik_I_)
   ├──001_Vorlesung
   │  ├──001_Skripte
   │  │  ├──Ordnernummer_LSFKurzbezeichnung_(Römischenummer)_Typ_Initialien
   │  │  └──(Bsp.: 014_PHYS_I_S_TN.pdf) 
   │  ├──002_Praesentationen
   │  ├──003_Vorlesungsvideos
   │  └──004_Zusammenfassungen
   │     ├──Ordnernummer_LSFKurzbezeichnung_(Römischenummer)_Typ_Initialien
   │     └──(Bsp.: 014_PHYS_I_ZF_TC.pdf)
   ├──002_Labore
   │  └──0XX_Beschreibung (Bsp.: 001_polsches_Rad)
   │     ├──Ordnernummer_LSFKurzbezeichnung_(Römischenummer)_Typ_LaborNr_Typ_(Initialien)
   │     └──(Bsp.: 014_PHYS_I_L_001_LSG_TC.pdf)
   ├──003_Uebungen
   │  └──001_Anki
   │     ├──Ordnernummer_LSFKurzbezeichnung_(Römischenummer)_Typ_Initialien
   │     └──(Bsp.: 014_PHYS_I_A_TC.pdf)
   ├──004_Klausuren
   │  ├──Ordnernummer_LSFKurzbezeichnung_(Römischenummer)_Typ_Jahr_Semester_(Initialien)
   │  └──(Bsp.: 014_PHYS_I_K_2023_SS_TC.pdf)
   └──005_Tutorium
      ├──Ordnernummer_LSFKurzbezeichnung_(Römischenummer)_Typ_Jahr_Semester_Initialien
      └──(Bsp.: 014_PHYS_I_T_2023_SS_TN)
         ├──(Bsp.: 014_PHYS_I_T_AUF_2023_01_01_TN.pdf)
         └──(Bsp.: 014_PHYS_I_T_LSG_2023_01_01_TN)
```
### Legende
- A = Anki Deck
- K = Klausur
  - SS = Sommersemester
  - WS = Wintersemester
- L = Labor
- M = Mindmap
- S = Skript
- T = Tutorium
  - LSG = Lösung
  - AUF = Aufgabe
- UE = Übung
- V = Video
- ZF = Zusammenfassung

## Erfahrungsberichte 
In jedem [Wiki](https://gitlab.com/thulm/et/s1/-/wikis/home) des Semesters oder Schwerpunktens findest du Erfahrungsberichte zu den jeweiligen Modulen. Teile auch gerne deine Erfahrungen und Tipps.

# Admins
- Inhaber: Tim Nachbauer (TN, @darulin007, nachti01@thu.de, Alumni) 
- Admin: Thorben Carl (TC, @ThorbenCarl, thorben.carl@gmail.com, ET6/ET8d)

## Semesterbetreuer
1. ? @darulin007 @ThorbenCarl
2. Lukas Meixner (LM, @meixnerluk, lukas_meixner@gmx.de, ET2)
3. Daniel Gläser (DG, @DanielGlaeser, glaeda01@thu.de, ET3/6d)
4. Daniela Ebenhoch (DE, @Dani_13_03, ebenda01@thu.de, ET4)
- Die Semesterbetreuer wachsen mit den Semestern mit. Schwerpunkte werden von den Admins betreut.